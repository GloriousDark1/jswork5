"use strict"

function createNewUser() {

    this.firstName = prompt("Назвіть своє ім'я ?");
    while (this.firstName === "") {
        this.firstName = prompt("Назвіть своє ім'я ?");
    }

    this.lastName = prompt("Яке ваше прізвище ?");
    while (this.lastName === "") {
        this.lastName = prompt("Яке ваше прізвище ?");
    }
    this.getLogin = function () {
        let newLogin = this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        return newLogin;
    }

}

let newObj = new createNewUser();
console.log(`${newObj.getLogin()}`);
